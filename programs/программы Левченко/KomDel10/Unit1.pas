unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ksTLB, ComObj, StdCtrls, ksConstTLB;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    OpenDialog1: TOpenDialog;
    Button7: TButton;
    SaveDialog1: TSaveDialog;
    Button8: TButton;
    Button9: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  kompas: KompasObject;
  Document2D: ksDocument2D;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
try
  kompas:= KompasObject(CreateOleObject('Kompas.Application.5'));
  kompas.Visible:=true;
except
end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  kompas.Quit();
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  Form1.Close;
end;

procedure TForm1.Button4Click(Sender: TObject);
var
  DocumentParam: ksDocumentParam;
begin
  DocumentParam:= ksDocumentParam(kompas.GetParamStruct(ko_DocumentParam));
end;

procedure TForm1.Button5Click(Sender: TObject);
var
  Document2D: ksDocument2D;
  DocumentParam: ksDocumentParam;
begin
  DocumentParam:=ksDocumentParam(kompas.GetParamStruct(ko_DocumentParam));
  DocumentParam.Init();
  DocumentParam.type_:=ksDocumentDrawing;
  Document2D:=ksDocument2D(kompas.Document2D);
  Document2D.ksCreateDocument(DocumentParam);
end;

procedure TForm1.Button6Click(Sender: TObject);
var

  DocumentParam: ksDocumentParam;
begin
  OpenDialog1.DefaultExt:='frw';
  OpenDialog1.Execute;
  OpenDialog1.InitialDir:=ExtractFilePath(Application.ExeName);
  DocumentParam:=ksDocumentParam(kompas.GetParamStruct(ko_DocumentParam));
  DocumentParam.Init();
  DocumentParam.type_:=ksDocumentDrawing;
  Document2D:=ksDocument2D(kompas.Document2D);
  Document2D.ksOpenDocument(OpenDialog1.FileName,false);
  //ExtractFilePath(Application.ExeName)
end;

procedure TForm1.Button7Click(Sender: TObject);
begin
  SaveDialog1.DefaultExt:='frw';
  SaveDialog1.InitialDir:=ExtractFilePath(Application.ExeName);
  SaveDialog1.Execute;
  Document2D.ksSaveDocument(SaveDialog1.FileName);
end;

procedure TForm1.Button8Click(Sender: TObject);
const
  SHEET_OPTIONS_EX = 4;
var
  DocumentParam: ksDocumentParam;
  StandartSheet: ksStandartSheet;
  SheetPar : ksSheetPar;
  str: string;
begin
  kompas:=KompasObject(CreateOleObject('Kompas.Application.5'));
  DocumentParam:=ksDocumentParam(kompas.GetParamStruct(ko_DocumentParam));
  DocumentParam.type_:=1;
  DocumentParam.regime := 0;
  SheetPar := ksSheetPar(DocumentParam.GetLayoutParam());
  str:= kompas.ksSystemPath(0)+'\graphic.lyt';
  SheetPar.layoutName := str;
  SheetPar.shtType := 1;
  StandartSheet := ksStandartSheet(SheetPar.GetSheetParam());
  StandartSheet.direct := false;
  StandartSheet.format:=4;
  StandartSheet.multiply := 1;
  Document2D:=ksDocument2D(kompas.Document2D);
  Document2D.ksCreateDocument(DocumentParam);
  kompas.Visible:=true;
end;

procedure TForm1.Button9Click(Sender: TObject);
const
  SHEET_OPTIONS_EX = 4;
var
  DocumentParam: ksDocumentParam;
  StandartSheet: ksStandartSheet;
  SheetPar : ksSheetPar;
  str: string;
TextItemParam: ksTextItemParam;
Stamp: ksStamp;
begin
  kompas:=KompasObject(CreateOleObject('Kompas.Application.5'));
  DocumentParam:=ksDocumentParam(kompas.GetParamStruct(ko_DocumentParam));
  DocumentParam.type_:=1;
  DocumentParam.regime := 0;
  SheetPar := ksSheetPar(DocumentParam.GetLayoutParam());
  str:= kompas.ksSystemPath(0)+'\graphic.lyt';
  SheetPar.layoutName := str;
  SheetPar.shtType := 1;
  StandartSheet := ksStandartSheet(SheetPar.GetSheetParam());
  StandartSheet.direct := false;
  StandartSheet.format:=4;
  StandartSheet.multiply := 1;
  Document2D:=ksDocument2D(kompas.Document2D);
  Document2D.ksCreateDocument(DocumentParam);
  kompas.Visible:=true;

TextItemParam:=ksTextItemParam(kompas.GetParamStruct(ko_TextItemParam));
Stamp:=ksStamp(Document2D.GetStamp());
Stamp.ksOpenStamp();
Stamp.ksColumnNumber(1);
TextItemParam.s:='������';
Stamp.ksTextLine(TextItemParam);
Stamp.ksColumnNumber(2);
TextItemParam.s:='����������� �������';
Stamp.ksTextLine(TextItemParam);
Stamp.ksColumnNumber(3);
TextItemParam.s:='����� 45';
Stamp.ksTextLine(TextItemParam);
Stamp.ksColumnNumber(110);
TextItemParam.s:='�������� �.�.';
Stamp.ksTextLine(TextItemParam);
Stamp.ksCloseStamp();
Document2D.ksText(100,100,0,5,0,0,'1.HRC50');
end;

end.
