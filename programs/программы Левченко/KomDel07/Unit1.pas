unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ksTLB, ComObj, StdCtrls, ksConstTLB;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    OpenDialog1: TOpenDialog;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  kompas: KompasObject;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
try
  kompas:= KompasObject(CreateOleObject('Kompas.Application.5'));
  kompas.Visible:=true;
except
end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  kompas.Quit();
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  Form1.Close;
end;

procedure TForm1.Button4Click(Sender: TObject);
var
  DocumentParam: ksDocumentParam;
begin
  DocumentParam:= ksDocumentParam(kompas.GetParamStruct(ko_DocumentParam));
end;

procedure TForm1.Button5Click(Sender: TObject);
var
  Document2D: ksDocument2D;
  DocumentParam: ksDocumentParam;
begin
  DocumentParam:=ksDocumentParam(kompas.GetParamStruct(ko_DocumentParam));
  DocumentParam.Init();
  DocumentParam.type_:=ksDocumentDrawing;
  Document2D:=ksDocument2D(kompas.Document2D);
  Document2D.ksCreateDocument(DocumentParam);
end;

procedure TForm1.Button6Click(Sender: TObject);
var
  Document2D: ksDocument2D;
  DocumentParam: ksDocumentParam;
begin
  OpenDialog1.Execute;
  DocumentParam:=ksDocumentParam(kompas.GetParamStruct(ko_DocumentParam));
  DocumentParam.Init();
  DocumentParam.type_:=ksDocumentDrawing;
  Document2D:=ksDocument2D(kompas.Document2D);
  Document2D.ksOpenDocument(OpenDialog1.FileName,false);
  //ExtractFilePath(Application.ExeName)
end;

end.
